<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
	}

	public function send(){
		$url = 'https://portalpulsa.com/api/connect/';
        
        $header = array(
        'portal-userid: P90246',
        'portal-key: 508bca558e38bd2f786cc0ab5ab4fb39', // lihat hasil autogenerate di member area
        'portal-secret: 95bef5b598f7f4014e27b3e11a8d4596088ff496aad2ef1b928e9f24b97c6a49', // lihat hasil autogenerate di member area
        );
        
        $data = array(
        'inquiry' => 'I', // konstan
        'code' => 'I5', // kode produk
        'phone' => '089673402881', // nohp pembeli
        'trxid_api' => 'xxxx', // Trxid / Reffid dari sisi client
        'no' => '1', // untuk isi lebih dari 1x dlm sehari, isi urutan 1,2,3,4,dst
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        
        echo $result;
	}
}
