<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$s = $_GET['h'];

		$end = substr($s, -3);

		if($end < 500){
			$sel = 500-$end;
			if($sel<300){
				echo $s+$sel+500;
			}else{
				echo $sel.'==';
				echo $s+$sel;
			}
			
		}else{
			$x = $end - 500; //125
			$xx = 500 - $x;

			if($xx<300){
				echo $s+$xx+500;
			}else{
				echo $xx.'==';
				echo $s+$xx;
			}
		}
	}
}
